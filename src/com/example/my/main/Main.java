package com.example.my.main;

import com.example.my.connection.Ats;
import com.example.my.input.ConsoleInput;
import com.example.my.input.Input;
import com.example.my.phone.AbstractPhoneFactory;
import com.example.my.phone.ConsolePhoneFactory;
import com.example.my.phone.Phone;
import com.example.my.phone.PhoneCreationException;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static Input input;
    private AbstractPhoneFactory factory;
    private List<Phone> phones = new ArrayList<>();

    private Main() {
        input = new ConsoleInput();
        factory = new ConsolePhoneFactory(input);
    }

    public static void main(String[] args) {
        new Main().start();
    }

    private void start() {
        String prompt = "1 - Create phone\n2 - Phone connect/disconnect\n3 - Call\n4 - Message\nM - Phone list\nX - Exit";
        while (true) {
            switch (input.inputString(prompt).toLowerCase()) {
                case "1":
                    createPhone();
                    break;
                case "2":
                    phoneConnection();
                    break;
                case "3":
                    call();
                    break;
                case "4":

                    break;
                case "m":
                    showPhones();
                    break;
                case "x":
                default:
                    System.out.println("Bye");
                    return;
            }
        }
    }

    private void call() {
        System.out.println("Select outgoing phone");
        Phone from = selectPhone();
        if (from == null) {
            return;
        }
        System.out.println("Select receiving phone");
        Phone to = selectPhone();
        if (to == null) {
            return;
        }
        if (from.equals(to)) {
            System.out.println("Phone can't call itself");
        }
        if (Ats.getInstance().link(from, to)) {
            System.out.println("Call successful");
        } else {
            System.out.println("Call failed");
        }
    }

    private void phoneConnection() {
        Phone phone = selectPhone();
        if (phone == null) {
            System.out.println("Action cancelled");
            return;
        }
        if (Ats.getInstance().disconnectPhone(phone)) {
            System.out.println("Phone disconnected");
        } else {
            Ats.getInstance().connectPhone(phone);
            System.out.println("Phone connected");
        }
    }

    private Phone selectPhone() {
        if (phones.size() == 0) {
            System.out.println("There are no phones created");
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Please select Phone:\n");
        for (int i = 0; i < phones.size(); i++) {
            sb.append(i + 1);
            sb.append(" - ");
            sb.append(phones.get(i));
            sb.append("\n");
        }
        Integer selection = input.inputInteger(sb.toString());
        if (selection == null) {
            System.out.println("Incorrect input");
            return null;
        } else if (selection < 1 || selection > phones.size()) {
            System.out.println("Incorrect number");
            return null;
        } else {
            return phones.get(selection - 1);
        }
    }

    private void createPhone() {
        String[] models = factory.getPhoneModelList();
        StringBuilder sb = new StringBuilder();
        sb.append("Please select model: \n");
        for (int i = 0; i < models.length; i++) {
            sb.append(i + 1);
            sb.append(" - ");
            sb.append(models[i]);
            sb.append("\n");
        }
        sb.append("C - back");
        String input = this.input.inputString(sb.toString()).toLowerCase();
        int modelIndex = 0;
        try {
            modelIndex = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            if (!input.equalsIgnoreCase("c")) {
                System.out.println("Incorrect input");
            }
            return;
        }
        if (modelIndex < 0 || modelIndex > models.length) {
            System.out.println("Incorrect number");
            return;
        }
        try {
            Phone phone = factory.createPhone(models[modelIndex - 1]);
            generateNumber(phone);
            phones.add(phone);
            System.out.println("Phone created");
        } catch (PhoneCreationException e) {
            System.out.println("Internal error");
        }
    }

    private void generateNumber(Phone phone) {
        phone.setNumber(String.valueOf(1000 + ((int) (Math.random() * 9000))));
    }

    private void showPhones() {
        if (phones.size() == 0) {
            System.out.println("There are no phones created");
            return;
        }
        for (Phone phone : phones) {
            System.out.println(phone + " isConnected: " + Ats.getInstance().isConnected(phone));
        }
        System.out.println();
    }
}
