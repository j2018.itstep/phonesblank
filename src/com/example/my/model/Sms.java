package com.example.my.model;

import java.util.Date;
import java.util.Objects;

public class Sms {
    private String numberFrom;
    private String numberTo;
    private String text;
    private Date timeSent;

    public Sms() {
    }

    public Sms(String numberFrom, String text) {
        this.numberFrom = numberFrom;
        this.text = text;
    }

    public String getNumberFrom() {
        return numberFrom;
    }

    public void setNumberFrom(String numberFrom) {
        this.numberFrom = numberFrom;
    }

    public String getNumberTo() {
        return numberTo;
    }

    public void setNumberTo(String numberTo) {
        this.numberTo = numberTo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sms sms = (Sms) o;
        return numberFrom.equals(sms.numberFrom) &&
                numberTo.equals(sms.numberTo) &&
                text.equals(sms.text) &&
                timeSent.equals(sms.timeSent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberFrom, numberTo, text, timeSent);
    }

    @Override
    public String toString() {
        return "Sms{" +
                "numberFrom='" + numberFrom + '\'' +
                ", numberTo='" + numberTo + '\'' +
                ", text='" + text + '\'' +
                ", timeSent=" + timeSent +
                '}';
    }
}
