package com.example.my.connection;

public interface Connector {
    String sendMessage();

    void receiveMessage(String message);
}
