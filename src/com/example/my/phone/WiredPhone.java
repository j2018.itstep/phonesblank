package com.example.my.phone;

public abstract class WiredPhone extends Phone {
    protected WiredPhone(double weight) {
        super(weight);
    }

    @Override
    public void ring() {
        System.out.println("brinng");
    }
}
