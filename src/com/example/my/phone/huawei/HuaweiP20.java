package com.example.my.phone.huawei;

import com.example.my.phone.IPolySound;
import com.example.my.phone.InternetAccess;
import com.example.my.phone.SmartPhone;

public class HuaweiP20 extends SmartPhone {
    private static final double weight = 290;

    public HuaweiP20() {
        super(weight);
    }

    @Override
    protected InternetAccess createInternetAccess() {
        return new HuaweiP20InternetAccess();
    }

    @Override
    protected IPolySound createRingtone() {
        return new HuaweiP20Ringtone();
    }

    @Override
    protected IPolySound createNotification() {
        return new HuaweiP20Notification();
    }

    private static class HuaweiP20Ringtone implements IPolySound {

        @Override
        public void playSound() {
            System.out.println("HuaweiP20 ringing");
        }
    }

    private static class HuaweiP20Notification implements IPolySound {

        @Override
        public void playSound() {
            System.out.println("HuaweiP20 notification");
        }
    }

    private static class HuaweiP20InternetAccess implements InternetAccess {

        @Override
        public void sendData() {

        }

        @Override
        public void receiveData() {

        }
    }
}
