package com.example.my.phone.cisco;

import com.example.my.phone.WiredPhone;

public class Cisco7931G extends WiredPhone {
    private static final double weight = 788;

    public Cisco7931G() {
        super(weight);
    }
}
