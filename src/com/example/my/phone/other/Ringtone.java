package com.example.my.phone.other;

public enum Ringtone {
    RINGTONE_STANDARD("blip-blip"), RINGTONE_LOW("pi-pi"), RINGTONE_HIGH("boom-boom");

    private String sound;

    Ringtone(String sound) {
        this.sound = sound;
    }

    public String getSound() {
        return sound;
    }
}
