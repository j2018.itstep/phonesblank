package com.example.my.phone;

public abstract class SmartPhone extends GsmPhone {
    private InternetAccess internetAccess;

    protected SmartPhone(double weight) {
        super(weight);
        this.internetAccess = createInternetAccess();
    }

    protected abstract InternetAccess createInternetAccess();

    public InternetAccess getInternetAccess() {
        return internetAccess;
    }

    public void setInternetAccess(InternetAccess internetAccess) {
        this.internetAccess = internetAccess;
    }
}
