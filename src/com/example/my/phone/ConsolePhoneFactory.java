package com.example.my.phone;

import com.example.my.input.Input;
import com.example.my.phone.cisco.Cisco7931G;
import com.example.my.phone.huawei.HuaweiP20;
import com.example.my.phone.nokia.Nokia3310;

public class ConsolePhoneFactory extends AbstractPhoneFactory {
    private String[] phoneModels = {
            Cisco7931G.class.getSimpleName(),
            Nokia3310.class.getSimpleName(),
            HuaweiP20.class.getSimpleName()};
    private Input input;

    public ConsolePhoneFactory(Input input) {
        this.input = input;
    }

    @Override
    public Phone createPhone(String modelName) throws PhoneCreationException {
        if (modelName.equalsIgnoreCase(phoneModels[0])) {
            return new Cisco7931G();
        } else if (modelName.equalsIgnoreCase(phoneModels[1])) {
            return new Nokia3310();
        } else if (modelName.equalsIgnoreCase(phoneModels[2])) {
            return new HuaweiP20();
        }
        throw new NoSuchModelException(modelName);
    }

    @Override
    public String[] getPhoneModelList() {
        return phoneModels;
    }
}
