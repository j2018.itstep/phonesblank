package com.example.my.phone;

public abstract class AbstractPhoneFactory {

    public abstract Phone createPhone(String modelName) throws PhoneCreationException;

    public abstract String[] getPhoneModelList();
}
