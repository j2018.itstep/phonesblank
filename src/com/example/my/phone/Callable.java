package com.example.my.phone;

import com.example.my.connection.Connector;

public interface Callable {
    Connector incomingCall();

    Connector outgoingCall(String number);
}
